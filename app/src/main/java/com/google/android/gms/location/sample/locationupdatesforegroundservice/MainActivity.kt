/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gms.location.sample.locationupdatesforegroundservice

import android.Manifest
import android.app.AlertDialog
import android.content.*
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.preference.PreferenceManager
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.sample.locationupdatesforegroundservice.DataUser.Companion.preffs
import com.google.android.gms.location.sample.locationupdatesforegroundservice.LocationUpdatesService.LocalBinder
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Model.FinalizaDomicilio
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Model.GetData
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Model.Resultado
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Repo.ServiceBuilder
import com.google.android.gms.location.sample.locationupdatesforegroundservice.utils.InterfaceFinishDomicilio
import com.google.android.gms.location.sample.locationupdatesforegroundservice.utils.InterfaceGetData
import com.google.android.gms.location.sample.locationupdatesforegroundservice.utils.InterfaceStartDomicilio
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main1.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.*
import kotlin.math.log


class MainActivity : AppCompatActivity(), OnSharedPreferenceChangeListener {
    // The BroadcastReceiver used to listen from broadcasts from the service.
    private var myReceiver: MyReceiver? = null

    // A reference to the service used to get location updates.
   var mService: LocationUpdatesService? = null

    // Tracks the bound state of the service.
    var mBound = false

    // UI elements.
    private var mRequestLocationUpdatesButton: Button? = null
    private var mRemoveLocationUpdatesButton: Button? = null

    // Monitors the state of the connection to the service.
    private val mServiceConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as LocalBinder
            mService = binder.service
            mBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }
    lateinit var prefs:SharedPreferences
    var sharedPreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myReceiver = MyReceiver()
        setContentView(R.layout.activity_main)
        prefs = getSharedPreferences("Preferencias", Context.MODE_PRIVATE)
        sharedPreferences = getSharedPreferences("valor_id", Context.MODE_PRIVATE)
        // Check that the user hasn't revoked permissions by going to Settings.
        if (Utils.requestingLocationUpdates(this)) {
            if (!checkPermissions()) {
                requestPermissions()
            }
        }
        //esta funcion resfresca y trae los domicilios
        swipeRefresh.setOnRefreshListener{
            showName()
        }
        swipeRefresh.setProgressBackgroundColorSchemeResource(R.color.ic_launcher_background);

        showName()//esta funcion carga los domicilios al entrar a la app
        Log.i("prefre result ", preffs.getName())
    }

    override fun onBackPressed() {
        //este metodo es para que no haga nada al darle atras en el telefono
    }


    //esta funcion crea la opcion de cerrar session
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu,menu)
        return true
    }

    //esta funcion limpia las credenciales guardadas en las preff y devuelve al login
    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.cerrar_session -> {
                deleteToken()
               // preffs.wipe()
                //startActivity(Intent(this, LoginActivity::class.java))

            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteToken(){
        val db = FirebaseFirestore.getInstance()
        db.collection(Constants.COLL_USERS)
            .document(preffs.getName())
            .collection(Constants.COLL_TOKENS).get()
            .addOnSuccessListener { documents ->
                for (document in documents){
                    db.collection(Constants.COLL_USERS)
                        .document(preffs.getName())
                        .collection(Constants.COLL_TOKENS)
                        .document(document.id).delete().addOnSuccessListener {
                            finish()
                            preffs.wipe()
                            startActivity(Intent(this, LoginActivity::class.java))

                        }
                }

            }

    }

    //funcion que llama la funcion para traer los domicilios
    fun showName(){
        val bundle = intent.extras
        //var id = bundle?.get("INTENT_ID") as Int
        var id = preffs.getIdmo()

        //Log.e("id", id.toString())
        getRetrofit(id)
        swipeRefresh.isRefreshing = false
    }

    //trae y llena la lista con los domicilios asignados
    private fun getRetrofit(id: Int) {
        //val textView : TextView = findViewById(R.id.recicleview)
        //val intent = Intent(this, MainActivity::class.java)
        val request = ServiceBuilder.buildService(InterfaceGetData::class.java)
        val call = request.Get_Data(id)

        call.enqueue(object : Callback<List<GetData>> {
            override fun onFailure(call: Call<List<GetData>>, t: Throwable) {
                Log.e("CallbackFail",t.message.toString())
            }

            override fun onResponse(call: Call<List<GetData>>, response: Response<List<GetData>>) {
                Log.e("Login Response","$response")
                val usuario = response.body()
                if (usuario != null) {
                    if (usuario[0].dm_id == 0){
                        //Toast.makeText(this@ViewMain,"Sin Domicilios",Toast.LENGTH_LONG).show()
                        alert()

                        recicleview.adapter?.notifyDataSetChanged()
                        recicleview.adapter = null
                    }else{
                        //alert("bienvenido", "${usuario!!.usu_nombres}")
                        //intent.putExtra("INTENT_USUARIO", usuario.cli_id)
                        //intent.putExtra("INTENT_ID", usuario.cli_dir)
                        //textView.text= usuario.cli_dir
                        // startActivity(intent)

                        recicleview.layoutManager = LinearLayoutManager(this@MainActivity)
                        recicleview.adapter = AdapterData(usuario,this@MainActivity)
                    }
                }


            }

        })
    }

    //mensaje cuando no encuentra domicilios
    private fun alert(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No Tienes Domicilios Asignados")
        builder.setIcon(R.drawable.ic_baseline_block_24)
        //builder.setMessage(msg2)
        /* builder.setPositiveButton("yes") { _: DialogInterface, _: Int ->
             finish()
         }*/
        builder.setNegativeButton("Aceptar") { _: DialogInterface, _: Int -> }
        builder.show()

    }

    public override fun onStart() {
        super.onStart()

        PreferenceManager.getDefaultSharedPreferences(this)
            .registerOnSharedPreferenceChangeListener(this)
        mRequestLocationUpdatesButton =
            findViewById<View>(R.id.request_location_updates_button) as Button
        mRemoveLocationUpdatesButton =
            findViewById<View>(R.id.remove_location_updates_button) as Button
        mRequestLocationUpdatesButton!!.setOnClickListener {
            if (!checkPermissions()) {
                requestPermissions()
            } else {
                mService!!.requestLocationUpdates()
            }
        }
        mRemoveLocationUpdatesButton!!.setOnClickListener { mService!!.removeLocationUpdates() }

        // Restore the state of the buttons when the activity (re)launches.
        setButtonsState(Utils.requestingLocationUpdates(this))

        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        bindService(
            Intent(this, LocationUpdatesService::class.java), mServiceConnection,
            BIND_AUTO_CREATE
        )
    }

    fun inicia(){
        mService!!.requestLocationUpdates()
        startDomicilio()
        showName()
    }

    fun fin(){
            MaterialAlertDialogBuilder(this)
                .setTitle("Desea Finalizar el Domicilo?")
                .setMessage("Se quitara de la lista el Domicilo!")
                .setNegativeButton("NO"){
                    dialog, which ->
                   showSnackbar("No Se hizo nada!")
                }
                .setPositiveButton("SI"){
                    dialog, which ->
                    mService!!.removeLocationUpdates()
                    finalizaDomicilio()
                    showSnackbar("Se Finalizó el domicilio")
                }

                .show()
/*
        val builder = AlertDialog.Builder(this)

               builder.setMessage("Desea Finalizar el Domicilio?")
                   .setCancelable(false)
                   .setPositiveButton("Si", DialogInterface.OnClickListener{
                           dialog, which ->
                       mService!!.removeLocationUpdates()
                       finalizaDomicilio()
                   })
                   .setNegativeButton("No", DialogInterface.OnClickListener{
                       dialog, which ->
                   })
                   builder.show()

               builder.setTitle("Deceas Finalizar el domicilio?")
               builder.setIcon(R.drawable.ic_baseline_clear_24)
               //builder.setMessage(msg2)
               builder.setPositiveButton("Si") { _: DialogInterface, _: Int ->
                   mService!!.removeLocationUpdates()
                   finalizaDomicilio()
                }
               builder.setNegativeButton("No") { _: DialogInterface, _: Int -> }
               builder.show()*/
    }

    fun showSnackbar(msg: String){
       Snackbar.make(root_layout,msg,Snackbar.LENGTH_SHORT).show()


    }

    fun iniciaTelefono(){//inicia el telefono con el telefono del cliente
        val num = preffs.getTelCliente()
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Uri.encode(num)))
        startActivity(intent)
    }


    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(this).registerReceiver(
            myReceiver!!,
            IntentFilter(LocationUpdatesService.Companion.ACTION_BROADCAST)
        )
    }

    override fun onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myReceiver!!)
        super.onPause()
    }

    override fun onStop() {
        if (mBound) {
            // Unbind from the service. This signals to the service that this activity is no longer
            // in the foreground, and the service can respond by promoting itself to a foreground
            // service.
            unbindService(mServiceConnection)
            mBound = false
        }
        PreferenceManager.getDefaultSharedPreferences(this)
            .unregisterOnSharedPreferenceChangeListener(this)
        super.onStop()
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            Snackbar.make(
                findViewById(R.id.activity_main),
                R.string.permission_rationale,
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.ok) { // Request permission
                    ActivityCompat.requestPermissions(
                        this@MainActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_PERMISSIONS_REQUEST_CODE
                    )
                }
                .show()
        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(
                this@MainActivity, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                mService!!.requestLocationUpdates()
            } else {
                // Permission denied.
                setButtonsState(false)
                Snackbar.make(
                    findViewById(R.id.activity_main),
                    R.string.permission_denied_explanation,
                    Snackbar.LENGTH_INDEFINITE
                )
                    .setAction(R.string.settings) { // Build intent that displays the App settings screen.
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts(
                            "package",
                            BuildConfig.APPLICATION_ID, null
                        )
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    }
                    .show()
            }
        }
    }

    /**
     * Receiver for broadcasts sent by [LocationUpdatesService].
     */
    private inner class MyReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val location =
                intent.getParcelableExtra<Location>(LocationUpdatesService.Companion.EXTRA_LOCATION)
            /*if (location != null) {
                Toast.makeText(
                    this@MainActivity, Utils.getLocationText(location),
                    Toast.LENGTH_SHORT
                ).show()
            }*/

            var id = preffs.getId_dm()
            var latitud = location?.latitude
            var longitud = location?.longitude
            var dm_codigo = preffs.getCodigo()
            var mo_nombre = preffs.getMo_nombre()
            var mo_telefono = preffs.getMo_telefono()
            var vh_placa =  preffs.getVh_placa()
            var nombreFoto = preffs.getFoto()
            Log.e("MENSAJE", "$id")


           Utils.saveFirebase(latitud, longitud,dm_codigo,mo_nombre,mo_telefono,vh_placa,nombreFoto)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, s: String) {
        // Update the buttons state depending on whether location updates are being requested.
        if (s == Utils.KEY_REQUESTING_LOCATION_UPDATES) {
            setButtonsState(
                sharedPreferences.getBoolean(
                    Utils.KEY_REQUESTING_LOCATION_UPDATES,
                    false
                )
            )
        }
    }

    private fun setButtonsState(requestingLocationUpdates: Boolean) {
        if (requestingLocationUpdates) {
            mRequestLocationUpdatesButton!!.isEnabled = false
            mRemoveLocationUpdatesButton!!.isEnabled = true
        } else {
            mRequestLocationUpdatesButton!!.isEnabled = true
            mRemoveLocationUpdatesButton!!.isEnabled = false
        }
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName

        // Used in checking for runtime permissions.
        private const val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

        fun finalizaDomicilio(){
            var idd = preffs.getId_dm2()
            val request = ServiceBuilder.buildService(InterfaceFinishDomicilio::class.java)
            val call = request.finalizaDomicilio(idd)
            call.enqueue(object :Callback<FinalizaDomicilio>{
                override fun onFailure(call: Call<FinalizaDomicilio>, t: Throwable) {
                    Log.e("CallbackFail",t.message.toString())
                    Log.e("Error", "id $idd")
                }
                override fun onResponse(call: Call<FinalizaDomicilio>, response: Response<FinalizaDomicilio>) {
                    Log.e("Login Response","$response")
                    //val fdomicilio = response.body()
                    showName()
                }

            })


        }


    fun startDomicilio(){
        var id_dm = preffs.getId_dm()
        val retrofit = Retrofit.Builder()
            .baseUrl("https://domicilioslcr.com/domicilios_ver2/webservices/")
            //.addCallAdapterFactory(object : RxJavaCallAdapterFactory())
            //.addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

        // Create Service
        val service = retrofit.create(InterfaceStartDomicilio::class.java)

        // Create JSON using JSONObject
        val jsonObject = JSONObject()
        jsonObject.put("id_dm", id_dm)

        // Convert JSONObject to String
        val jsonObjectString = jsonObject.toString()

        // Create RequestBody ( We're not using any converter, like GsonConverter, MoshiConverter e.t.c, that's why we use RequestBody )
        val requestBody = jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())
        Log.e("BODY", "${jsonObjectString}")

        val request = ServiceBuilder.buildService(InterfaceStartDomicilio::class.java)
        val call = request.startDomicilio(requestBody)

        call.enqueue(object :Callback<Resultado>{
            override fun onFailure(call: Call<Resultado>, t: Throwable) {
                Log.e("ERROR", "Faile" )
            }

            override fun onResponse(call: Call<Resultado>, response: Response<Resultado>) {
                Log.e("SUCCESS", "${response.isSuccessful}" )
                Log.e("ACTIVA","$response")

            }

        })
    }



    }



















