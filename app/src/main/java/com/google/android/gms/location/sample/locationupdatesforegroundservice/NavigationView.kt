package com.google.android.gms.location.sample.locationupdatesforegroundservice

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class NavigationView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_view)
        var id = intent.getStringExtra("VALOR")

        //Launch google maps
        val gmmIntentUri =
            Uri.parse("google.navigation:q=$id&mode=l")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")

        startActivity(mapIntent)


    }
}