package com.google.android.gms.location.sample.locationupdatesforegroundservice

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.Call
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.google.android.gms.common.api.Response
import com.google.android.gms.location.sample.locationupdatesforegroundservice.DataUser.Companion.preffs
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Model.Usuario
import com.google.android.gms.location.sample.locationupdatesforegroundservice.Repo.ServiceBuilder
import com.google.android.gms.location.sample.locationupdatesforegroundservice.utils.InterfaceUsuario
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import javax.security.auth.callback.Callback

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val btnIniciar = findViewById<Button>(R.id.bt_iniciar)
        btnIniciar.setOnClickListener{validar()}

        accessToDetail()
    }

    fun accessToDetail(){
        if (preffs.getName().isNotEmpty()){
            startActivity(Intent(this, MainActivity::class.java))
        }
    }

    /*  aca stan las validaciones de los campos que no esten vacios chivo*/
    private fun validar(){
        val usuario = findViewById<EditText>(R.id.username)
        val usuario2 = usuario.text.toString()
        val pass = findViewById<EditText>(R.id.password)
        val password = pass.text.toString()

        if (usuario2.isNotEmpty())
        {
            if (password.isNotEmpty()){
                getRetrofit(usuario2.trim(),password.trim())
            }else{
                alert("Contraseña Vacía", "Debes Escribir una Contraseña")
                pass.requestFocus()
            }
        }else {
            alert("Usuario Vacío", "Debes Escribir un Nombre de Usuario")
            usuario.requestFocus()
        }

    }

    /* esta funcion es para el mensaje de alerta como me dijiste con dialog */
    private fun alert(msg: String, msg2: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(msg)
        builder.setMessage(msg2)
        /* builder.setPositiveButton("yes") { _: DialogInterface, _: Int ->
             finish()
         }*/
        builder.setNegativeButton("Aceptar") { _: DialogInterface, _: Int -> }
        builder.show()

    }

    private fun getRetrofit(usuario2: String, password: String) {
        val intent = Intent(this, MainActivity::class.java)
        // para guardar el token para notificar
        val preferences = PreferenceManager.getDefaultSharedPreferences(this)

        val request = ServiceBuilder.buildService(InterfaceUsuario::class.java)
        val call = request.login(usuario2,password)

        call.enqueue(object : retrofit2.Callback<Usuario> {
            override fun onFailure(call: retrofit2.Call<Usuario>, t: Throwable) {
                Log.e("CallbackFail",t.message.toString())
            }

            override fun onResponse(call: retrofit2.Call<Usuario>, response: retrofit2.Response<Usuario>) {
                Log.e("Login Response","$response")
                val usuario = response.body()
                if (usuario != null) {
                    if (usuario.usu_nombre == null){
                        alert("Usuario o Contraseña Erroneos", "Por favor Verificar")
                    }else{
                        //alert("bienvenido", "${usuario!!.usu_nombres}")
                        preffs.saveName(usuario.usu_nombre)
                        preffs.saveIdmo(usuario.usu_id_mo)

                        // esto me servira para guardar en la base de datos de real time de firebase
                        val token = preferences.getString(Constants.PROP_TOKEN, null)
                        token?.let {
                            val db = FirebaseFirestore.getInstance()
                            val tokenMap = hashMapOf(Pair(Constants.PROP_TOKEN, token))

                            db.collection(Constants.COLL_USERS)
                                .document(preffs.getName())
                                .collection(Constants.COLL_TOKENS)
                                .add(tokenMap)
                                .addOnSuccessListener {
                                    Log.i("Registrado token", token)
                                  /*
                                    Esto lo quite por razones que si otro usuario inicia sesion
                                    pueda obtener la notificacion con su propio token
                                    preferences.edit {
                                        putString(Constants.PROP_TOKEN, null)
                                            .apply()
                                    } */
                                }
                                .addOnFailureListener {
                                    Log.i("No se Registro TOken", token)
                                }

                        }
                        //Log.i("user", usuario2.toString())
                        startActivity(intent)
                        /* intent.putExtra("INTENT_USUARIO", usuario.usu_nombre)
                         intent.putExtra("INTENT_ID", usuario.usu_id_mo)
                         startActivity(intent)*/
                    }
                }


            }

        })


    }
}