/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.android.gms.location.sample.locationupdatesforegroundservice

import android.content.ContentValues
import android.content.Context
import android.location.Location
import android.preference.PreferenceManager
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

internal object Utils {
    const val KEY_REQUESTING_LOCATION_UPDATES = "requesting_location_updates"
    val db = Firebase.firestore
    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The [Context].
     */
    fun requestingLocationUpdates(context: Context?): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(context)
            .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false)
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    fun setRequestingLocationUpdates(context: Context?, requestingLocationUpdates: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
            .apply()
    }

    /**
     * Returns the `location` object as a human readable string.
     * @param location  The [Location].
     */
    fun getLocationText(location: Location?): String {
        return if (location == null) "Unknown location" else "(" + location.latitude + ", " + location.longitude + ")"
    }


    fun saveFirebase(
        latitude: Double?,
        longitude: Double?,
        dm_codigo: String,
        mo_nombre: String,
        mo_telefono: String,
        vh_placa: String,
        nombreFoto: String,

        ){
        val fecha = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = fecha.format(Date())
        val ip = "https://domicilioslcr.com"
        val urlfoto= ip+"/domicilios_ver2/fotos_motoristas/"+nombreFoto

        val user = hashMapOf(
            "latitude" to latitude,
            "longitude" to longitude,
            "nombre_moto" to mo_nombre,
            "placa_moto" to vh_placa,
            "telefono_moto" to mo_telefono,
            "ultima_actualizacion" to currentDate,
            "foto_moto" to urlfoto,
            "dm_codigo" to dm_codigo

        )
        //Log.e("codigo md", "$dm_codigo")

        db.collection("motos")
            .document("$dm_codigo")
            .set(user)
            .addOnSuccessListener {
                Log.d(ContentValues.TAG, "DocumentSnapshot added with ")
            }
            .addOnFailureListener { e ->
                Log.w(ContentValues.TAG, "Error adding document", e)
            }

    }

    fun getLocationTitle(context: Context): String {
        return context.getString(
            R.string.location_updated,
            DateFormat.getDateTimeInstance().format(Date())
        )
    }
}