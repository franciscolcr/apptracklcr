package com.google.android.gms.location.sample.locationupdatesforegroundservice

import android.app.Application

class DataUser: Application()  {
    companion object{
        lateinit var preffs: Prefs
    }

    override fun onCreate() {
        super.onCreate()
        preffs = Prefs(applicationContext)

    }
}