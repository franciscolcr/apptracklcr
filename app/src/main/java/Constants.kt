object Constants {
    // Collections in firebase
    const val COLL_USERS = "users"
    const val COLL_TOKENS = "tokens"
    // FCM
    const val PROP_TOKEN = "token"
    const val USER_LOGGUE = ""

}